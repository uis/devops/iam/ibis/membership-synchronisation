import logging
import os
import sys
import requests_oauthlib

from identitylib.identifiers import Identifier, IdentifierSchemes

from .types import InstMapping

INST_MAPPING_API_ROOT = 'https://api.apps.cam.ac.uk/institutions/mapping/v1/'

LOG = logging.getLogger(os.path.basename(sys.argv[0]))


def fetch_inst_mapping(session: requests_oauthlib.OAuth2Session) -> InstMapping:
    """
    Fetch institutional mapping dict from API Gateway. Use it to compile a mapping of Student
    Records Institution ids to Lookup instids

    """
    r = session.get(INST_MAPPING_API_ROOT)
    r.raise_for_status()
    inst_map: InstMapping = {}
    for datum in r.json().get('institutions', []):
        for i in datum.get('identifiers', []):
            try:
                id = Identifier.from_string(i, find_by_alias=True)
            except ValueError as e:
                LOG.warning(e)
                continue
            # Only interested in Student Records Institution ids
            if id.scheme == IdentifierSchemes.STUDENT_INSTITUTION:
                # Warn if there is a duplicate mapping and keep first (typically College)
                # rather than overwrite
                if inst_map.get(id.value) is not None:
                    LOG.warning("Institution %s already mapped to %s not %s",
                                id.value, inst_map.get(id.value), datum['instid'])
                else:
                    inst_map[id.value] = datum['instid']

    LOG.info('Fetched mapping for %s institutions.', len(inst_map))
    return inst_map
