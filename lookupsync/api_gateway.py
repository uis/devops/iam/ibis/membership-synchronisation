import requests_oauthlib
import oauthlib.oauth2

API_GATEWAY_TOKEN_URL = 'https://api.apps.cam.ac.uk/oauth2/v1/token'


def create_api_gateway_session(opts: dict) -> requests_oauthlib.OAuth2Session:
    """
    Create a requests session object authenticated to use the API Gateway via the credentials
    passed in lookup_sync_client_credentials.

    """
    client_id = opts['--gateway-client-id']
    client_secret = opts.get('--gateway-client-secret')
    client_secret_from = opts.get('--gateway-client-secret-from')
    if client_secret_from is not None:
        with open(client_secret_from) as fobj:
            client_secret = fobj.read().strip()

    client = oauthlib.oauth2.BackendApplicationClient(client_id=client_id)
    oauth = requests_oauthlib.OAuth2Session(client=client)
    oauth.fetch_token(
        token_url=API_GATEWAY_TOKEN_URL, client_id=client_id, client_secret=client_secret)
    return oauth
