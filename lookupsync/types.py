# Convenient type aliases and definitions
from typing import TypedDict

# Institution Ids in both Student API and Lookup are strings and we use a mapping between them
StudentAPIInstId = str
LookupInstId = str
InstMapping = dict[StudentAPIInstId, LookupInstId]
InstIdSet = set[LookupInstId]


# Student USNs and names are strings, and for logging we keep a mapping between them
StudentId = str
StudentName = str
StudentMapping = dict[StudentId, StudentName]
StudentIdSet = set[StudentId]


# Group names as strings, and we use a mapping of group names to sets of student ids
GroupName = str
StudentsByGroup = dict[GroupName, StudentIdSet]
GroupSet = set[GroupName]


# A group change consists of a set of student ids to add and a set to remove
class GroupChange(TypedDict):
    add: StudentIdSet
    remove: StudentIdSet


# A collection of group changes is a mapping of group name to changes
GroupChanges = dict[GroupName, GroupChange]
