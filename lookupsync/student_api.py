from typing import Optional
from collections.abc import Generator
import logging
import os
import sys
import datetime

import pydantic
import urllib.parse
import requests_oauthlib
from identitylib.identifiers import IdentifierSchemes

from .types import InstMapping, StudentMapping, StudentsByGroup
from .lookup import group_name

STUDENT_API_ROOT = 'https://api.apps.cam.ac.uk/university-student/v1alpha2/'

ACADEMIC_CAREER_MAPPING = {
    'UGRD': 'ug',
    'PGRD': 'pg',
}

LOG = logging.getLogger(os.path.basename(sys.argv[0]))


def get_students_by_group(
        session: requests_oauthlib.OAuth2Session,
        inst_map: InstMapping) -> tuple[StudentsByGroup, StudentMapping]:
    """
    Create a map from Lookup group name to sets of students within that institution with status
    matching career. Group names are formed from the Lookup instids and student identifiers
    are USNs

    Note that since students can be members of more than one institution, the sum of the lengths
    of each sets may not equal the length of the union of all of the sets.

    Also create mapping for student ids to names for logging.

    """
    students_by_group: StudentsByGroup = {}
    student_names_by_id: StudentMapping = {}

    # Capture ignored affiliations and careers
    ignored_affiliations = set()
    ignored_careers = set()

    # Fetch all students parsed as instances of Student.
    today = datetime.date.today()
    for s in fetch_all_students(session):
        # Add student to any institutions they are affiliated with.
        for a in s.affiliations:
            # Validate and normalise scheme
            try:
                aff_scheme = IdentifierSchemes.from_string(a.scheme, find_by_alias=True)
            except ValueError as e:
                LOG.warning(e)
                continue
            # Ignore non college/departmental affiliations.
            if aff_scheme != IdentifierSchemes.STUDENT_INSTITUTION:
                continue

            # Ignore expired or yet to be affiliations.
            if a.end is not None and a.end < today:
                continue
            if a.start is not None and a.start >= today:
                continue

            # Find the instid for this institution. Continue if there is no known mapping.
            instid = inst_map.get(a.value)
            if instid is None:
                ignored_affiliations.add(a.value)
                continue

            # Only those with appropriate Academic Career (status) values
            career = ACADEMIC_CAREER_MAPPING.get(a.status)
            if career is None:
                ignored_careers.add(a.status)
                continue

            # Ensure there is a set of student ids for this institution career group.
            student_ids = students_by_group.setdefault(group_name(instid, career), set())

            for i in s.identifiers:
                # Validate and normalise scheme
                try:
                    id_scheme = IdentifierSchemes.from_string(i.scheme, find_by_alias=True)
                except ValueError as e:
                    LOG.warning(f'Identifier: {i.value} ({a.value}) - "{e}"')
                    continue
                # Only identifiers from Student Records Person scheme (USNs)
                if id_scheme != IdentifierSchemes.USN:
                    continue

                # Add USN and remember name for USN
                student_ids.add(i.value)
                student_names_by_id[i.value] = f'{s.forenames} {s.surname}'

    # Report ignored values (possibly missing from inst mapping or career mapping above)
    if ignored_affiliations:
        LOG.info('Ignored Affiliations:')
        for a in sorted(ignored_affiliations):
            LOG.info(f'- {a}')
    if ignored_careers:
        LOG.info('Ignored Academic Careers:')
        for a in sorted(ignored_careers):
            LOG.info(f'- {a}')

    return students_by_group, student_names_by_id


# Student API schema

class StudentIdentifier(pydantic.BaseModel):
    """
    Identifier resource from Student API.

    """
    scheme: str
    value: str


class StudentAffiliation(pydantic.BaseModel):
    """
    Affiliation resource from Student API.

    """
    end: Optional[datetime.date]
    scheme: str
    start: Optional[datetime.date]
    status: str
    value: str


class Student(pydantic.BaseModel):
    """
    Student resource from Student API.

    """
    affiliations: list[StudentAffiliation]
    forenames: str
    identifiers: list[StudentIdentifier]
    namePrefixes: str
    surname: str


def fetch_all_students(
        session: requests_oauthlib.OAuth2Session) -> Generator[list[Student], None, None]:
    """
    Fetch all students from the students API. *Generates* a list of Student resources.

    """
    next_url = urllib.parse.urljoin(STUDENT_API_ROOT, 'students')
    while next_url is not None:
        LOG.info('Fetching %s...', next_url)
        r = session.get(next_url)
        r.raise_for_status()
        data = r.json()
        yield from map(Student.parse_obj, data.get('results', []))
        next_url = data.get('next')
