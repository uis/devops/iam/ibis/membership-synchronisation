"""
Lookup Membership Synchronisation Tool

Usage:
    lookupsync (-h | --help)
    lookupsync student-inst-members --gateway-client-id=CLIENT_ID --lookup-username=USERNAME
        ( --gateway-client-secret=CLIENT_SECRET | --gateway-client-secret-from=PATH )
        ( --lookup-password=PASSWORD | --lookup-password-from=PATH )
        [--quiet] [--debug] [--lookup-test | --lookup-local] [--really-do-this]

Options:
    -h, --help                      Show a brief usage summary.

    --quiet                         Reduce logging verbosity.
    --debug                         Log debugging information.

    --gateway-client-id=CLIENT_ID   Client id to use when authenticating to Gateway.
    --gateway-client-secret=CLIENT_SECRET
                                    Client secret to use when authenticating to Gateway.
    --gateway-client-secret-from=PATH
                                    Load API Gateway client secret from PATH. Leading and trailing
                                    whitespace is trimmed. This is preferable to passing secrets on
                                    the command line.

    --lookup-username=USERNAME      Username to use when authenticating to Lookup.
    --lookup-password=PASSWORD      Password to use when authenticating to Lookup.
    --lookup-password-from=PATH     Load password to use when authenticating to Lookup from PATH.
                                    Leading and trailing whitespace is trimmed. This is preferable
                                    to passing secrets on the command line.

    --lookup-test                   Use Lookup test instance instead of production
    --lookup-local                  Use local instance of Lookup (developers only)

    --really-do-this                Actually attempt to update lookup group memberships, otherwise
                                    just output what would have been changed.

Operations:
    The student-inst-members operation will synchronise membership of institutions from CamSIS'
    affiliation records.
"""
import logging
import os
import sys
import docopt

import ibisclient

from .api_gateway import create_api_gateway_session
from .inst_mapping import fetch_inst_mapping
from .student_api import get_students_by_group
from .lookup import (
    create_lookup_connection, compare_with_lookup_groups, check_usns_in_lookup,
    filter_usns_from_changes, create_lookup_groups, strip_groups_missing_insts,
    update_lookup_groups)

STUDENT_API_ROOT = 'https://api.apps.cam.ac.uk/university-student/v1alpha2/'
INST_MAPPING_API_ROOT = 'https://api.apps.cam.ac.uk/institutions/mapping/v1/'

ACADEMIC_CAREER_MAPPING = {
    'UGRD': 'ug',
    'PGRD': 'pg',
}

LOG = logging.getLogger(os.path.basename(sys.argv[0]))


def main(argv=None):
    """
    Entrypoint for tool. Passed argv or None to use sys.argv.

    """
    # In principle __doc__ could be None so use f-strings to ensure input to docopt is always a
    # string.
    opts = docopt.docopt(f"{__doc__}", argv=argv)

    # Configure logging
    logging.basicConfig(
        level=logging.DEBUG if opts["--debug"] else
        logging.WARN if opts["--quiet"] else logging.INFO
    )

    dry_run = not opts['--really-do-this']
    if dry_run:
        LOG.warning('Operating in dry-run mode - no changes will be made')

    if opts['student-inst-members']:
        _student_inst_members(opts, dry_run)


# OPERATIONS


def _student_inst_members(opts: dict, dry_run: bool):
    """
    Synchronise institutional memberships for students.

    """
    session = create_api_gateway_session(opts)

    # Fetch institutional mapping, a dict mapping Student Records Inst ids to Lookup instids
    inst_map = fetch_inst_mapping(session)

    # Sanity check
    if len(inst_map) == 0:
        raise RuntimeError('Failed to fetch any institutional mappings')

    # Build a map of Lookup group name to sets of students within that institution with status
    # matching career.
    students_by_group, student_names_by_id = get_students_by_group(session, inst_map)

    # Sanity check
    if len(students_by_group) == 0:
        raise RuntimeError('Failed to fetch any student records')

    # Log some stats
    LOG.info('Fetched record(s) for %s groups.', len(students_by_group))
    LOG.info('Total affiliation count: %s', sum(len(s) for s in students_by_group.values()))
    all_students = set()
    for s in students_by_group.values():
        all_students |= s
    LOG.info('Total student count: %s', len(all_students))

    ibis_conn = create_lookup_connection(opts)
    ibis_group_methods = ibisclient.GroupMethods(ibis_conn)
    ibis_person_methods = ibisclient.PersonMethods(ibis_conn)
    ibis_inst_methods = ibisclient.InstitutionMethods(ibis_conn)

    # Calculate changes and find missing groups
    (missing_groups, group_changes) = compare_with_lookup_groups(
            ibis_group_methods, students_by_group, student_names_by_id)

    # Find USNs (that are being added) that are not recognised by lookup
    all_usns_to_add = {
        usn
        for _, changes in group_changes.items()
        for usn in changes['add']
    }
    missing_usns = check_usns_in_lookup(ibis_person_methods, all_usns_to_add)

    # Filter missing USNs from group changes
    filter_usns_from_changes(group_changes, missing_usns, student_names_by_id)
    # No need to create groups if they no longer have members to add
    missing_groups = {
        group for group in missing_groups if group_changes.get(group)['add']
    }

    if missing_groups:
        # Create groups that couldn't be found
        missing_insts = create_lookup_groups(ibis_inst_methods, missing_groups, dry_run)
        if missing_insts:
            LOG.info('%s institution(s) not found', len(missing_insts))
            # Strip group changes for institutions that couldn't be found
            previous_count = len(group_changes)
            group_changes = strip_groups_missing_insts(group_changes, missing_insts)
            if previous_count != len(group_changes):
                LOG.info('%s groups(s) ignored as no matching institution found',
                         previous_count - len(group_changes))

    # Make changes to Lookup groups
    update_lookup_groups(ibis_group_methods, group_changes, student_names_by_id, dry_run)
