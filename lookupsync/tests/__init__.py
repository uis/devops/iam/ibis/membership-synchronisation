from contextlib import contextmanager
from tempfile import NamedTemporaryFile
import os

# Example Student Records institution value
SR_INST_VALUE = "SRID1"


@contextmanager
def temp_password_file(pwd: str):
    """
    Context manager for temporary file containing given string
    """
    try:
        file = NamedTemporaryFile(delete=False, mode='w')
        file_name = file.name
        file.write(pwd)
        file.close()
        yield file_name
    finally:
        if file_name is not None:
            os.unlink(file_name)


class MockResponse():
    def __init__(self, data: dict):
        self.data = data

    def raise_for_status(self):
        pass

    def json(self) -> dict:
        return self.data


class MockSession():
    def __init__(self, responses: dict[str, dict]):
        self.responses = responses
        self.urls_got = set()

    def get(self, url: str) -> MockResponse:
        assert url in self.responses.keys()
        self.urls_got.add(url)
        return MockResponse(self.responses[url])
