from typing import Optional, Union
import faker

from identitylib.identifiers import IdentifierSchemes

from lookupsync.student_api import ACADEMIC_CAREER_MAPPING
from . import SR_INST_VALUE


class StudentProvider(faker.providers.BaseProvider):
    """
    A faker provider for student data as provided by Student API

    """

    def student(self, **kwargs) -> dict[str, Union[str, list, dict]]:
        """
        A single fake student, overriding properties as necessary

        """
        return {
            k: kwargs.get(k) or getattr(self, k)(**kwargs)
            for k in ['namePrefixes', 'surname', 'forenames', 'affiliations', 'identifiers']
        }

    def namePrefixes(self, **kwargs) -> str:
        return self.generator.prefix().rstrip('.')

    def surname(self, **kwargs) -> str:
        return self.generator.last_name()

    def forenames(self, **kwargs) -> str:
        return self.generator.first_name() + (
            '' if self.generator.boolean(chance_of_getting_true=25)
            else (' ' + self.generator.first_name())
        )

    def affiliations(self, num_affiliations=1, **kwargs) -> list[dict[str, str]]:
        return [self.affiliation(**kwargs) for _ in range(num_affiliations)]

    def identifiers(self, num_identifiers=1, **kwargs) -> list[dict[str, str]]:
        return [self.identifier(**kwargs) for _ in range(num_identifiers)]

    def affiliation(self, **kwargs) -> dict[str, str]:
        value, scheme = (kwargs.get('affiliation_id') or self.affiliation_id(**kwargs)).split('@')
        start, end = kwargs.get('affiliation_period') or self.affiliation_period(**kwargs)
        return {
            'value': value,
            'scheme': scheme,
            'start': start,
            'end': end,
            'status': kwargs.get('status') or self.affiliation_status(**kwargs),
        }

    def affiliation_id(self, **kwargs) -> str:
        # Provides example affiliation (in test mapping) with expected scheme
        return f'{SR_INST_VALUE}@{IdentifierSchemes.STUDENT_INSTITUTION}'

    def affiliation_period(self, **kwargs) -> tuple[Optional[str], Optional[str]]:
        # Unspecified start and end dates
        return (None, None)

    def affiliation_status(self, **kwargs) -> str:
        return self.random_element(ACADEMIC_CAREER_MAPPING.keys())

    def identifier(self, **kwargs) -> dict[str, str]:
        value, scheme = (kwargs.get('identifier_id') or self.identifier_id(**kwargs)).split('@')
        return {
            'value': value,
            'scheme': scheme,
        }

    def identifier_id(self, **kwargs) -> str:
        # Provides random person with expected scheme
        return f'{self.random_int(10000000, 99999999)}@{IdentifierSchemes.USN}'
