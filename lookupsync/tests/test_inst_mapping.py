from unittest import TestCase
import logging

from . import MockSession, SR_INST_VALUE
from lookupsync.inst_mapping import fetch_inst_mapping, INST_MAPPING_API_ROOT

INST_MAPPING_RESPONSE = {
    'institutions': [
        {
            "instid": "FOO",
            "identifiers": [
                # Duplicate of "instid" - not actually used in sync
                "FOO@insts.lookup.cam.ac.uk",
                # current scheme
                f"{SR_INST_VALUE}@institution.v1.student-records.university.identifiers.cam.ac.uk",
                # depreciated but still valid id
                "SRID2@institution.v1.student.university.identifiers.cam.ac.uk",
                # ignored id but using valid scheme
                "HRID1@institution.v1.human-resources.university.identifiers.cam.ac.uk",
                # invalid scheme
                "BADID1@institution.v1.fake.university.identifiers.cam.ac.uk",
            ]
        }, {
            "instid": "BAR",
            "identifiers": [
                # current scheme
                "SRID100@institution.v1.student-records.university.identifiers.cam.ac.uk",
            ]
        }, {
            "instid": "BAZ",
            "identifiers": [
                # current scheme
                "SRID200@institution.v1.student-records.university.identifiers.cam.ac.uk",
            ]
        },
    ]
}
INST_MAPPING_EXPECTED = {
    SR_INST_VALUE: 'FOO',
    'SRID2': 'FOO',
    'SRID100': 'BAR',
    'SRID200': 'BAZ',
}


class InstMappingTest(TestCase):

    def test_fetch_inst_mapping(self):
        """
        fetch_inst_mapping gets the response from the institution mapping API and
        converts the 'institutions' list returned to a dict of only Student Record
        institution ids to Lookup instids

        """
        session = MockSession({INST_MAPPING_API_ROOT: INST_MAPPING_RESPONSE})

        with self.assertLogs(level=logging.WARNING) as captured:
            inst_map = fetch_inst_mapping(session)

        # Mapping matches expectations
        self.assertEqual(inst_map, INST_MAPPING_EXPECTED)

        # Warning given for invalid scheme
        self.assertIn(
            'Invalid identifier scheme institution.v1.fake.university.identifiers.cam.ac.uk',
            [r.getMessage() for r in captured.records])
