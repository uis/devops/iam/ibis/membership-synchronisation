from unittest.mock import MagicMock, patch

from . import temp_password_file
from lookupsync.api_gateway import create_api_gateway_session, API_GATEWAY_TOKEN_URL


@patch('oauthlib.oauth2.BackendApplicationClient')
@patch('requests_oauthlib.OAuth2Session')
def test_create_api_gateway_session_var(oauthlib_mock: MagicMock, backend_client_mock: MagicMock):
    """
    create_api_gateway_session will request a token from the API Gateway Token URL with the
    given client_id and client_secret

    """
    session = create_api_gateway_session({
        '--gateway-client-id': 'TEST_ID',
        '--gateway-client-secret': 'TEST_SECRET',
    })
    # Backend client created with client id
    backend_client_mock.assert_called_once_with(client_id='TEST_ID')
    backend_instance = backend_client_mock.return_value
    # OAuth2Session created with created backend client
    oauthlib_mock.assert_called_once_with(client=backend_instance)
    oauth2session = oauthlib_mock.return_value
    # fetch_token called with client id and secret
    oauth2session.fetch_token.assert_called_with(
        token_url=API_GATEWAY_TOKEN_URL, client_id='TEST_ID', client_secret='TEST_SECRET'
    )
    assert session == oauth2session


@patch('oauthlib.oauth2.BackendApplicationClient')
@patch('requests_oauthlib.OAuth2Session')
def test_create_api_gateway_session_file(oauthlib_mock: MagicMock, backend_client_mock: MagicMock):
    """
    create_api_gateway_session will request a token from the API Gateway Token URL with the
    given client_id, and client_secret fetched from given file

    """
    with temp_password_file('SECRET_IN_FILE') as secret_file:
        session = create_api_gateway_session({
            '--gateway-client-id': 'TEST_ID',
            '--gateway-client-secret-from': secret_file,
        })
    # Backend client created with client id
    backend_client_mock.assert_called_once_with(client_id='TEST_ID')
    backend_instance = backend_client_mock.return_value
    # OAuth2Session created with created backend client
    oauthlib_mock.assert_called_once_with(client=backend_instance)
    oauth2session = oauthlib_mock.return_value
    # fetch_token called with client id and secret
    oauth2session.fetch_token.assert_called_with(
        token_url=API_GATEWAY_TOKEN_URL, client_id='TEST_ID', client_secret='SECRET_IN_FILE'
    )
    assert session == oauth2session
