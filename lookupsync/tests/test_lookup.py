import logging
from unittest.mock import MagicMock, patch
import pytest

import ibisclient

from . import temp_password_file
from lookupsync.lookup import (
    check_usns_in_lookup, create_lookup_connection, compare_with_lookup_groups,
    create_lookup_groups, filter_usns_from_changes, update_lookup_groups,
    GROUP_DESCRIPTION, TRANSACTION_COMMENT)


@patch('ibisclient.createConnection')
def test_create_lookup_connection_var(ibis_connect_mock: MagicMock):
    """
    create_lookup_connection will create an ibisclient.IbisClientConnection with the
    given username and password

    """
    ibis_conn = create_lookup_connection({
        '--lookup-username': 'TEST_USER',
        '--lookup-password': 'TEST_PASSWORD',
        '--lookup-test': False,
        '--lookup-local': False,
    })
    # createConnection called once
    ibis_connect_mock.assert_called_once()
    ibis_connect_instance = ibis_connect_mock.return_value
    # Connection has username and password set
    ibis_connect_instance.set_username.assert_called_once_with('TEST_USER')
    ibis_connect_instance.set_password.assert_called_once_with('TEST_PASSWORD')
    assert ibis_conn == ibis_connect_instance


@patch('ibisclient.createConnection')
def test_create_lookup_connection_file(ibis_connect_mock: MagicMock):
    """
    create_lookup_connection will create an ibisclient.IbisClientConnection with the
    given username, and password fetched from given file

    """
    with temp_password_file('PASSWORD_IN_FILE') as password_file:
        ibis_conn = create_lookup_connection({
            '--lookup-username': 'TEST_USER',
            '--lookup-password-from': password_file,
            '--lookup-test': False,
            '--lookup-local': False,
        })
    # createConnection called once
    ibis_connect_mock.assert_called_once()
    ibis_connect_instance = ibis_connect_mock.return_value
    # Connection has username and password set
    ibis_connect_instance.set_username.assert_called_once_with('TEST_USER')
    ibis_connect_instance.set_password.assert_called_once_with('PASSWORD_IN_FILE')
    assert ibis_conn == ibis_connect_instance


@patch('ibisclient.createTestConnection')
@patch('ibisclient.createConnection')
def test_create_lookup_test_connection(
        ibis_connect_mock: MagicMock, ibis_test_connect_mock: MagicMock):
    """
    create_lookup_connection will create an ibisclient.IbisClientTestConnection with the
    given username and password, if '--lookup-test' also set in `opts`

    """
    ibis_conn = create_lookup_connection({
        '--lookup-username': 'TEST_USER',
        '--lookup-password': 'TEST_PASSWORD',
        '--lookup-test': True,
        '--lookup-local': False,
    })
    # createConnection is not called
    ibis_connect_mock.assert_not_called()
    # but createTestConnection is instead
    ibis_test_connect_mock.assert_called_once()
    ibis_connect_instance = ibis_test_connect_mock.return_value
    # Connection has username and password set
    ibis_connect_instance.set_username.assert_called_once_with('TEST_USER')
    ibis_connect_instance.set_password.assert_called_once_with('TEST_PASSWORD')
    assert ibis_conn == ibis_connect_instance


@pytest.fixture
def mock_students_by_group():
    """
    An example dict of Lookup groups to sets of USNs
    """
    return {
        'foo-sis-ug': {
            '123456789',
            '234567890',
        },
        'foo-sis-pg': {
            '345678901',
            '456789012',
        },
        'bar-sis-ug': {
            '123456789',
            '456789012',
            '567890123',
        }
    }


@pytest.fixture
def mock_student_names_by_id():
    """
    An example dict of student ids to their visible names
    """
    return {
        '123456789': 'Emil Tatton',
        '234567890': 'Pen Goddard',
        '345678901': 'Lucia Bennington',
        '456789012': 'Lynette Massey',
        '567890123': 'Dorthy Adams',
    }


@pytest.fixture
def mock_lookup_group_members(mock_students_by_group, mock_student_names_by_id):
    """
    An example dict of Lookup groups to list of ibisclient.IbisPerson records matching
    students_by_group mock
    """
    def _make_ibis_person(usn: str):
        person = ibisclient.IbisPerson()
        identifier = ibisclient.IbisIdentifier()
        setattr(identifier, 'scheme', 'usn')
        setattr(identifier, 'value', usn)
        setattr(person, 'identifiers', [identifier])
        setattr(person, 'visibleName', mock_student_names_by_id.get(usn))
        return person

    return {
        group: [_make_ibis_person(usn) for usn in members]
        for group, members in mock_students_by_group.items()
    }


# Mock ibisclient.GroupMethods
class MockGroupMethods:
    def __init__(self, groups: list[dict[str, ibisclient.IbisPerson]] = []) -> None:
        self.groups = groups
        self.updates = {}

    def getDirectMembers(self, group, _):
        return self.groups[group]  # will raise an exception like ibisclient for missing groups

    def getGroup(self, group):
        return self.groups.get(group)

    def updateDirectMembers(self, group, to_add, to_remove, _):
        self.updates[group] = {'to_add': to_add, 'to_remove': to_remove}


def test_compare_with_lookup_groups(mock_students_by_group, mock_lookup_group_members):
    """
    Matching students_by_group and getDirectMembers responses results in no missing groups or
    changes to groups

    """
    # Perform comparison
    mock_group_methods = MockGroupMethods(mock_lookup_group_members)
    missing_groups, group_changes = compare_with_lookup_groups(
        mock_group_methods, mock_students_by_group, {})

    # Empty results
    assert missing_groups == set()
    assert group_changes == dict()


def test_compare_with_lookup_groups_missing_group(
        mock_students_by_group, mock_lookup_group_members):
    """
    A group in students_by_group but not in getDirectMembers responses results in the group being
    reported as missing

    """
    # Add a group in mock_student_by_groups that is not in mock_lookup_group_members
    NEW_GROUP = 'baz-sis-pg'
    USN_TO_ADD = '999999999'
    assert mock_lookup_group_members.get(NEW_GROUP) is None
    mock_students_by_group[NEW_GROUP] = {USN_TO_ADD}

    # Perform comparison
    mock_group_methods = MockGroupMethods(mock_lookup_group_members)
    missing_groups, group_changes = compare_with_lookup_groups(
        mock_group_methods, mock_students_by_group, {})

    # Just new group reported missing and all members appear in changes to be added
    assert missing_groups == {NEW_GROUP}
    assert group_changes == {NEW_GROUP: {'add': {USN_TO_ADD}, 'remove': set()}}


def test_compare_with_lookup_groups_changes(
        mock_students_by_group, mock_lookup_group_members, mock_student_names_by_id):
    """
    Group changes to add and/or remove members are reported

    """
    GROUP_TO_ADD_ONLY_TO = 'foo-sis-ug'
    GROUP_TO_REMOVE_ONLY_FROM = 'foo-sis-pg'
    GROUP_TO_ADD_TO_AND_REMOVE_FROM = 'bar-sis-ug'
    USN_TO_ADD = '999999999'
    NAME_TO_ADD = 'Kacie Elwyn'
    USN_TO_REMOVE = '456789012'
    NAME_TO_REMOVE = 'Lynette Massey'

    # Assert groups exist
    assert GROUP_TO_ADD_ONLY_TO in mock_students_by_group
    assert GROUP_TO_REMOVE_ONLY_FROM in mock_students_by_group
    assert GROUP_TO_ADD_TO_AND_REMOVE_FROM in mock_students_by_group

    # Assert USN student to add don't already exist in appropriate groups
    assert USN_TO_ADD not in mock_students_by_group[GROUP_TO_ADD_ONLY_TO]
    assert USN_TO_ADD not in mock_students_by_group[GROUP_TO_ADD_TO_AND_REMOVE_FROM]

    # Assert USN of student to remove does already exist in appropriate groups
    assert USN_TO_REMOVE in mock_students_by_group[GROUP_TO_REMOVE_ONLY_FROM]
    assert USN_TO_REMOVE in mock_students_by_group[GROUP_TO_ADD_TO_AND_REMOVE_FROM]

    # Make changes
    mock_student_names_by_id[USN_TO_ADD] = NAME_TO_ADD
    mock_students_by_group[GROUP_TO_ADD_ONLY_TO].add(USN_TO_ADD)
    mock_students_by_group[GROUP_TO_ADD_TO_AND_REMOVE_FROM].add(USN_TO_ADD)
    del mock_student_names_by_id[USN_TO_REMOVE]
    mock_students_by_group[GROUP_TO_REMOVE_ONLY_FROM].remove(USN_TO_REMOVE)
    mock_students_by_group[GROUP_TO_ADD_TO_AND_REMOVE_FROM].remove(USN_TO_REMOVE)

    # Perform comparison
    mock_group_methods = MockGroupMethods(mock_lookup_group_members)
    missing_groups, group_changes = compare_with_lookup_groups(
        mock_group_methods, mock_students_by_group, mock_student_names_by_id)

    # No missing groups but appropriate changes reported
    assert missing_groups == set()
    assert group_changes == {
        GROUP_TO_ADD_ONLY_TO: {'add': {USN_TO_ADD}, 'remove': set()},
        GROUP_TO_REMOVE_ONLY_FROM: {'add': set(), 'remove': {USN_TO_REMOVE}},
        GROUP_TO_ADD_TO_AND_REMOVE_FROM: {'add': {USN_TO_ADD}, 'remove': {USN_TO_REMOVE}},
    }
    # Student to remove has their name mapped from their USN
    assert mock_student_names_by_id.get(USN_TO_REMOVE) == NAME_TO_REMOVE


def test_update_lookup_groups(caplog):
    """
    update_lookup_groups calls updateDirectMembers method of ibisclient.GroupMethods instance
    for each group with matching changes

    """
    # Group changes with just adds, just removes and both
    CHANGES = {
        'foo-sis-ug': {'add': {'111', '222'}, 'remove': set()},
        'foo-sis-pg': {'add': set(), 'remove': {'333', '444'}},
        'bar-sis-ug': {'add': {'555', '666'}, 'remove': {'777', '888'}},
    }
    NAMES_BY_ID = {str(x)*3: f'Name {x}' for x in range(1, 9)}

    mock_group_methods = MockGroupMethods()

    # Make update calls (dry-run)
    update_lookup_groups(mock_group_methods, CHANGES, NAMES_BY_ID, True)

    # No updateDirectMembers calls made
    assert mock_group_methods.updates == {}

    # Make update calls (not dry-run)
    mock_group_methods = MockGroupMethods()
    with caplog.at_level(logging.INFO):
        update_lookup_groups(mock_group_methods, CHANGES, NAMES_BY_ID, False)

    # Each group was updated
    assert set(CHANGES.keys()) == set(mock_group_methods.updates.keys())

    for group, changes in CHANGES.items():
        # Expected call args are prefixed with 'usn/'
        expected_to_add = {f'usn/{usn}' for usn in changes['add']}
        expected_to_remove = {f'usn/{usn}' for usn in changes['remove']}
        # but are lists not sets
        assert isinstance(mock_group_methods.updates[group]['to_add'], list)
        assert isinstance(mock_group_methods.updates[group]['to_remove'], list)
        # however have matching contents
        assert expected_to_add == set(mock_group_methods.updates[group]['to_add'])
        assert expected_to_remove == set(mock_group_methods.updates[group]['to_remove'])

    # additions and removals logged with names
    all_expected_to_add = {usn for usn in changes['add'] for _, changes in CHANGES.items()}
    for usn in all_expected_to_add:
        assert f'adding usn/{usn} ({NAMES_BY_ID.get(usn)})' in caplog.text
    all_expected_to_remove = {usn for usn in changes['remove'] for _, changes in CHANGES.items()}
    for usn in all_expected_to_remove:
        assert f'removing usn/{usn} ({NAMES_BY_ID.get(usn)})' in caplog.text


# Mock ibisclient.InstitutionMethods
class MockInstitutionMethods:
    def __init__(self, insts: list[dict[str, ibisclient.IbisInstitution]] = []) -> None:
        self.insts = insts
        self.group_creations = {}
        self.conn = MagicMock()
        self.conn.username = 'TEST_USER'

    def getInst(self, inst):
        return self.insts.get(inst)

    def createGroup(self, instid, name, title, description,
                    managedBy=None, commitComment=None):
        # Should only be called with known institution
        assert instid in self.insts
        # Record all parts of the creation
        self.group_creations.setdefault(instid, []).append({
            'name': name,
            'title': title,
            'description': description,
            'managedBy': managedBy,
            'commitComment': commitComment,
        })


@pytest.fixture
def mock_instids_to_names():
    """
    An example set of Lookup institution instids

    """
    return {
        'FOO': 'Department of Foo',
        'BAR': 'Faculty of Bar',
        'MISC': 'School of Miscellaneous'
    }


@pytest.fixture
def mock_lookup_institutions(mock_instids_to_names):
    """
    An example dict of Lookup instids to ibisclient.IbisInstitution records matching those
    in mock_instids fixture

    """
    def _make_ibis_inst(instid: str, name: str):
        inst = ibisclient.IbisInstitution({'instid': instid})
        setattr(inst, 'name', name)
        return inst

    return {
        instid: _make_ibis_inst(instid, name)
        for instid, name in mock_instids_to_names.items()
    }


def test_create_lookup_groups(mock_lookup_institutions, mock_instids_to_names):
    """
    Groups in known institution get created. Those in unknown institutions don't
    get created and the institution is included in the returned set.

    """
    # New groups (2 for 1 known inst, 1 for another known inst, 1 for unknown inst)
    NEW_GROUPS = {'foo-sis-ug', 'foo-sis-pg', 'bar-sis-ug', 'bad-sis-pg'}
    EXPECTED_MISSING_INSTS = {'BAD'}
    EXPECTED_CREATED_GROUPS_BY_INST = {
        'FOO': {'foo-sis-ug', 'foo-sis-pg'},
        'BAR': {'bar-sis-ug'},
    }

    mock_inst_methods = MockInstitutionMethods(mock_lookup_institutions)

    # Make create calls (dry-run)
    create_lookup_groups(mock_inst_methods, NEW_GROUPS, True)

    # No createGroup calls made
    assert mock_inst_methods.group_creations == {}

    # Make create calls (not dry-run)
    missing_insts = create_lookup_groups(mock_inst_methods, NEW_GROUPS, False)

    # Set of missing institutions returned
    assert EXPECTED_MISSING_INSTS == missing_insts

    # Only groups with a known institution are created
    assert (
        set(EXPECTED_CREATED_GROUPS_BY_INST.keys()) ==
        set(mock_inst_methods.group_creations.keys())
    )

    for instid, groups in mock_inst_methods.group_creations.items():
        # Expected groups created for each institution
        assert (
            set(EXPECTED_CREATED_GROUPS_BY_INST[instid]) ==
            {group['name'] for group in groups}
        )
        inst_name = mock_instids_to_names[instid]
        for group in groups:
            # Each group would be created with:
            # ... a title containing institution name and (Under|Post)graduates
            assert inst_name in group['title']
            if group['name'][-2:] == 'ug':
                assert 'Undergraduates' in group['title']
            else:
                assert 'Postgraduates' in group['title']
            # ... a fixed description and commit message
            assert group['description'] == GROUP_DESCRIPTION
            assert group['commitComment'] == TRANSACTION_COMMENT
            # ... and be managed by the connection's user
            assert group['managedBy'] == 'TEST_USER'


@pytest.fixture
def mock_lookup_people(mock_student_names_by_id):
    """
    An example dict of USNs to ibisclient.IbisPerson records using student_names_by_id mock

    """
    def _make_ibis_person(usn: str, name: str):
        person = ibisclient.IbisPerson()
        identifier = ibisclient.IbisIdentifier()
        setattr(identifier, 'scheme', 'usn')
        setattr(identifier, 'value', usn)
        setattr(person, 'identifiers', [identifier])
        setattr(person, 'visibleName', name)
        return person

    return {
        usn: _make_ibis_person(usn, name)
        for usn, name in mock_student_names_by_id.items()
    }


# Mock ibisclient.PersonMethods
class MockPersonMethods:
    def __init__(self, people: dict[str, ibisclient.IbisPerson] = {}) -> None:
        self.people = people
        self.listCount = 0

    def listPeople(self, ids: list[str], fetch: str):
        self.listCount += 1
        assert fetch == 'all_identifiers'
        assert all([id[:4] == 'usn/' for id in ids])
        return [
            self.people.get(usn)
            for usn in [id[4:] for id in ids]
            if self.people.get(usn)
        ]


def test_check_usns_in_lookup(mock_lookup_people):
    """
    check_usns_in_lookup returns which USNs aren't recognised by Lookup for a
    given set

    """
    mock_person_methods = MockPersonMethods(mock_lookup_people)

    # All people present, no missing USNs
    missing_usns = check_usns_in_lookup(mock_person_methods, set(mock_lookup_people.keys()))
    assert missing_usns == set()

    # USNs not in mock_lookup_people are reported as missing
    ADDITIONAL_USNS = {'888', '999'}
    missing_usns = check_usns_in_lookup(
        mock_person_methods, set(mock_lookup_people.keys()) | ADDITIONAL_USNS)
    assert missing_usns == ADDITIONAL_USNS


@patch('lookupsync.lookup.CHUNK_SIZE', 2)
def test_check_usns_in_lookup_chunking(mock_lookup_people):
    """
    check_usns_in_lookup requests lists in chunks

    """
    mock_person_methods = MockPersonMethods(mock_lookup_people)

    # All people present, no missing USNs
    missing_usns = check_usns_in_lookup(mock_person_methods, set(mock_lookup_people.keys()))
    assert missing_usns == set()

    # Our sample size of 5 and chunk size of 2 will result in 3 calls to listPeople()
    assert mock_person_methods.listCount == 3


def test_filter_usns_from_changes(caplog):
    """
    filter_usns_from_changes removes additions to groups of missing USNs

    """
    # Group changes with just adds, just removes and both
    group_changes = {
        'foo-sis-ug': {'add': {'111', '222'}, 'remove': set()},
        'foo-sis-pg': {'add': set(), 'remove': {'333', '444'}},
        'bar-sis-ug': {'add': {'555', '666'}, 'remove': {'777', '888'}},
    }
    NAMES_BY_ID = {str(x)*3: f'Name {x}' for x in range(1, 9)}

    MISSING_USNS = {'111', '555'}

    # Remove 'add' changes for missing USNs
    with caplog.at_level(logging.INFO):
        filter_usns_from_changes(group_changes, MISSING_USNS, NAMES_BY_ID)

    assert group_changes == {
        'foo-sis-ug': {'add': {'222'}, 'remove': set()},
        'foo-sis-pg': {'add': set(), 'remove': {'333', '444'}},
        'bar-sis-ug': {'add': {'666'}, 'remove': {'777', '888'}},
    }

    # Only groups that had USNs filtered out are logged
    for group in {'foo-sis-ug', 'bar-sis-ug'}:
        assert f'Ignoring in group {group}' in caplog.text
    # All missing USNs logged
    for usn in MISSING_USNS:
        assert f'- usn/{usn} ({NAMES_BY_ID.get(usn)})' in caplog.text
