from collections.abc import Generator, Iterable
import os
import sys
import logging
import itertools

import ibisclient

from .types import (GroupChange, GroupChanges, GroupName, GroupSet,
                    InstIdSet, LookupInstId,
                    StudentId, StudentIdSet, StudentMapping, StudentsByGroup)

LOG = logging.getLogger(os.path.basename(sys.argv[0]))

# Make group 'career' suffix to group title suffix
GROUP_TITLE_MAPPING = {
    'ug': 'Undergraduates',
    'pg': 'Postgraduates',
}
# Fixed group description of created groups
GROUP_DESCRIPTION = (
    "Group synchronised with student affiliations in the University's Student Information System."
)

# Transaction comments for group creation and update
TRANSACTION_COMMENT = "SIS Synchronisation"

# Request lists of people from lookup in chunks to avoid overflowing querystring
CHUNK_SIZE = 10


def create_lookup_connection(opts: dict) -> ibisclient.IbisClientConnection:
    """
    Create an IbisClientConnection to either production or test Lookup API service, authenticated
    with username and password from commandline or file.

    """
    username = opts['--lookup-username']
    password = opts.get('--lookup-password')
    password_from = opts.get('--lookup-password-from')
    if password_from is not None:
        with open(password_from) as fobj:
            password = fobj.read().strip()

    if opts['--lookup-local']:
        LOG.info('Using local dev instance of Lookup')
        ibis_conn = ibisclient.createLocalConnection()
    elif opts['--lookup-test']:
        LOG.info('Using test instance of Lookup')
        ibis_conn = ibisclient.createTestConnection()
    else:
        ibis_conn = ibisclient.createConnection()

    ibis_conn.set_username(username)
    ibis_conn.set_password(password)
    return ibis_conn


def group_name(instid: LookupInstId, career: str) -> GroupName:
    """
    Create Lookup group name from institution id and academic career mapping

    >>> group_name('foo', 'ug')
    'foo-sis-ug'
    >>> group_name('BAR', 'pg')
    'bar-sis-pg'

    """
    return f'{instid.lower()}-sis-{career}'


def compare_with_lookup_groups(
        ibis_group_methods: ibisclient.GroupMethods,
        students_by_group: StudentsByGroup,
        student_names_by_id: StudentMapping) -> tuple[GroupSet, GroupChanges]:
    """
    Check each lookup group exists and compare its membership to supplied set.
    Provide set of missing Lookup groups, and necessary changes as a dict mapping
    group name to dict of sets of USNs to 'add' and 'remove'.

    """
    # calculate changes
    missing_groups: GroupSet = set()
    group_changes: GroupChanges = dict()
    for group, students in sorted(students_by_group.items()):
        # Check that group exists
        if ibis_group_methods.getGroup(group) is None:
            missing_groups.add(group)
            # Will want to add everyone after creating the group
            group_changes[group] = GroupChange(add=students, remove=set())
            LOG.info('Group "%s" needs creating with %s students', group, len(students))
            continue

        LOG.info('Group "%s" should have %s student(s):', group, len(students))

        # Get lookup direct membership
        members: list[ibisclient.IbisPerson] = ibis_group_methods.getDirectMembers(
            group, 'all_identifiers')

        # Get set of USNs from membership
        group_usns = {
            id.value
            for person in members if person.identifiers is not None
            for id in person.identifiers if id.scheme == 'usn'
        }
        LOG.info(
            '- Lookup has %s member(s) with %s USNs%s',
            len(members), len(group_usns),
            ' - mismatch with membership' if len(group_usns) != len(members) else ''
        )

        # Determine who to add and/or remove
        to_add = students - group_usns
        if to_add:
            LOG.info('- %s need%s adding', len(to_add), 's' if len(to_add) == 1 else '')
        to_remove = group_usns - students
        if to_remove:
            LOG.info('- %s need%s removing', len(to_remove), 's' if len(to_add) == 1 else '')
            # Add their names to our id to name mapping
            student_names_by_id.update({
                id.value: person.visibleName
                for person in members if person.identifiers is not None
                for id in person.identifiers if id.scheme == 'usn' and id.value in to_remove
            })

        if to_add or to_remove:
            group_changes[group] = GroupChange(add=to_add, remove=to_remove)

    LOG.info('%s group(s) need creating', len(missing_groups))
    LOG.info('%s group(s) need changes', len(group_changes))
    return (missing_groups, group_changes)


def chunker(n: int, ids: Iterable[StudentId]) -> Generator[tuple[int, StudentIdSet], None, None]:
    """
    Yield chunks of size `n` from `ids`. The last chunk may be smaller.
    If `n` is not greater than the size of `ids`, just yields the one chunk

    """
    if n <= 0:
        yield 0, set(ids)
    else:
        iterators = [iter(ids)] * n
        chunks = itertools.zip_longest(*iterators, fillvalue=None)
        # unfortunately we need to drop the Nones on the last element
        for i, chunk in enumerate(chunks):
            yield i, set(filter(None, chunk))


def check_usns_in_lookup(
        ibis_person_methods: ibisclient.PersonMethods,
        all_usns_to_add: StudentIdSet) -> StudentIdSet:
    """
    Check that the set of students to add have USNs recognised by Lookup.
    Return a list of those that don't.

    """
    matched_usns: StudentIdSet = set()
    LOG.info('Checking USNs exist in Lookup:')
    for idx, usns in chunker(CHUNK_SIZE, all_usns_to_add):
        people: list[ibisclient.IbisPerson] = ibis_person_methods.listPeople(
            [f'usn/{usn}' for usn in usns],
            'all_identifiers'
        )
        usns_of_people: StudentIdSet = {
            id.value
            for person in people if person.identifiers is not None
            for id in person.identifiers if id.scheme == 'usn'
        }
        LOG.info(f'- batch {idx} : missing {len(usns) - len(usns_of_people)} of {len(usns)} USNs')
        matched_usns |= usns_of_people

    return all_usns_to_add - matched_usns


def filter_usns_from_changes(
        group_changes: GroupChanges, missing_usns: StudentIdSet,
        student_names_by_id: StudentMapping
        ) -> None:
    """
    Filter USNs to add from the changes if they are in the missing USNs list.
    Log which USNs are ignored for each group.

    """
    for group, changes in group_changes.items():
        missing_in_group_add = {
            usn for usn in changes['add'] if usn in missing_usns
        }
        if not missing_in_group_add:
            continue
        LOG.info('Ignoring in group %s:', group)
        for usn in missing_in_group_add:
            LOG.info('- usn/%s (%s)', usn, student_names_by_id.get(usn, 'Unknown'))
        # remove ignored USNs from set to add
        changes['add'] -= missing_in_group_add


def update_lookup_groups(
        ibis_group_methods: ibisclient.GroupMethods,
        group_changes: GroupChanges,
        student_names_by_id: StudentMapping,
        dry_run: bool = True) -> None:
    """
    Log and update (if not a dry-run) group memberships

    """
    for group, changes in group_changes.items():
        if not changes['add'] and not changes['remove']:
            continue
        LOG.info('Updating %s:', group)
        for usn in changes['add']:
            LOG.info('- adding usn/%s (%s)', usn, student_names_by_id.get(usn, 'Unknown'))
        for usn in changes['remove']:
            LOG.info('- removing usn/%s (%s)', usn, student_names_by_id.get(usn, 'Unknown'))
        if dry_run:
            LOG.info('- skipping update in dry-run mode')
        else:
            ibis_group_methods.updateDirectMembers(
                group,
                [f'usn/{usn}' for usn in changes['add']],
                [f'usn/{usn}' for usn in changes['remove']],
                TRANSACTION_COMMENT,
            )


def create_lookup_groups(
        ibis_inst_methods: ibisclient.InstitutionMethods,
        groups_to_create: GroupSet,
        dry_run: bool = True) -> InstIdSet:
    """
    Log and create (if not a dry-run) lookup groups with names, titles, descriptions
    and the current lookup account as the only manager.
    Return a set of institution ids for institutions that couldn't be found.

    """
    # Get current authenticated lookup account
    managed_by = ibis_inst_methods.conn.username
    # Cache institution details so we can name, title and describe groups appropriately
    institutions: dict[LookupInstId, ibisclient.IbisInstitution] = {}
    # Compile a set of institutions we couldn't find
    missing_insts: InstIdSet = set()

    for group in sorted(groups_to_create):
        # split group name into instid and career (ug or pg)
        (instid, _, career) = group.split('-')
        instid = instid.upper()

        if instid not in institutions:
            # Make sure institution exists
            inst = ibis_inst_methods.getInst(instid)
            if inst is None:
                missing_insts.add(instid)
                LOG.warning('Institution "%s" not found for group "%s"', instid, group)
                continue
            institutions[instid] = inst

        inst_name = institutions[instid].name
        group_title = f'{inst_name} - SIS - {GROUP_TITLE_MAPPING[career]}'
        LOG.info('Creating group "%s"', group)
        LOG.info('- in institution "%s"', instid)
        LOG.info('- with title "%s"', group_title)
        if dry_run:
            LOG.info('- skipping creation in dry-run mode')
        else:
            ibis_inst_methods.createGroup(
                instid, group, group_title,
                GROUP_DESCRIPTION, managed_by, TRANSACTION_COMMENT)

    return missing_insts


def strip_groups_missing_insts(
        group_changes: GroupChanges, missing_insts: InstIdSet) -> GroupChanges:
    """
    Remove group changes that would belong to institutions that couldn't be found.

    >>> strip_groups_missing_insts(
    ...    {
    ...        'abc-sis-pg': {'add': {'123'}, 'remove': {'456'}},
    ...        'def-sis-ug': {'add': {'234'}, 'remove': {'567'}},
    ...    },
    ...    {'DEF', 'GHI'}
    ... )
    {'abc-sis-pg': {'add': {'123'}, 'remove': {'456'}}}

    """
    return {
        group: changes for group, changes in group_changes.items()
        if group.split('-')[0].upper() not in missing_insts
    }
