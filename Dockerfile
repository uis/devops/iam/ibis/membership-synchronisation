FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/python:3.9-slim

WORKDIR /usr/src/app

# Install specific requirements for the package.
RUN pip3 install --upgrade pip
ADD requirements.txt ./
RUN pip3 install tox && pip3 install -r requirements.txt

# Copy application source and install it. Use "-e" to avoid needlessly copying
# files into the site-packages directory.
ADD ./ ./
RUN pip3 install -e .

ENTRYPOINT ["lookupsync"]
