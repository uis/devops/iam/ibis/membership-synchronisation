# Lookup Membership Synchronisation Tool

Tool to query Lookup for CHRIS and CamSIS institutional membership.

> This tool is currently only has the one operation `student-inst-members` that is able to compare
> CamSIS student affiliations to Lookup group membership and update as appropriate. If the
> appropriate lookup group doesn't exist then it is created first.
>
> Additionally, an operation to do the same for CHRIS institution membership is yet to be
> implemented.

## Installation

The `lookupsync` tool can be installed via pip:

```console
pip3 install --user https://gitlab.developers.cam.ac.uk/uis/devops/iam/ibis/membership-synchronisation.git
```

## Usage

See the output from `--help` for usage.

## Programmatic use

This tool can also be called programmatically by importing the `main` function
and calling it with command line arguments:

```python
import lookupsync

lookupsync.main([
    'student-inst-members', '--gateway-client-id=ABCDEF',
    '--gateway-client-secret-from=/path/to/secret', ...
])
```
